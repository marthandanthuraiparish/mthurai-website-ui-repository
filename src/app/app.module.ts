import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './modules/site-module/home/home.component';
import { HeaderComponent } from '../app/modules/shared-components/header/header.component';
import { FooterComponent } from '../app/modules/shared-components/footer/footer.component';
import { SermonsComponent } from './sermons/sermons.component';
import { OwlModule } from 'ngx-owl-carousel';
import { HttpClientModule } from '@angular/common/http';
import { UpcomingeventsComponent } from './upcomingevents/upcomingevents.component';
import { EventsummaryComponent } from './modules/shared-components/eventsummary/eventsummary.component';
import { EventdetailsComponent } from './modules/shared-components/eventdetails/eventdetails.component';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    SermonsComponent,
    UpcomingeventsComponent,
    EventsummaryComponent,
    EventdetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    OwlModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
