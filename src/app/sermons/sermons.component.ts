import { Component, OnInit, ViewChild } from '@angular/core';
import { $, jQuery } from 'jquery';
import 'owl.carousel'
import { SermonsService } from './sermons.service';
import { Subscription } from 'rxjs';
@Component({
    selector: 'app-sermons',
    templateUrl: './sermons.component.html',
    styleUrls: ['./sermons.component.scss']
})
export class SermonsComponent implements OnInit {
    @ViewChild('owlElement') owlElement;
    title = '';
    SlideOptions = { items: 1, dots: true, nav: true, autoplay: true, autoplayTimeout: 5000 };
    CarouselOptions = { items: 3, dots: true, nav: true, autoplay: true, };
    subscription: Subscription;
    sermonsInfo: any;
    constructor(private sermonsService: SermonsService) { }

    ngOnInit(): void {
        this.fetchAllSermosInformation();
        this.dummySermonInfo();
    }

    fetchAllSermosInformation() {
        this.subscription = this.sermonsService.fetchAllSermosInformation().subscribe(response => {
            if (response) {
                this.sermonsInfo = response;
            }
        });
    }
    dummySermonInfo () {
       this.sermonsInfo = this.sermonsService.dummySermonInfo();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
