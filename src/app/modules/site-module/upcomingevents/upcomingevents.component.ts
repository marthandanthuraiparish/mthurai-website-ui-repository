import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-upcomingevents',
	templateUrl: './upcomingevents.component.html',
	styleUrls: ['./upcomingevents.component.scss']
})
export class UpcomingeventsComponent implements OnInit {
	upcommingEvent: any;
	constructor() { }

	ngOnInit(): void {
		this.fetchAllUpcommingEvents();
	}

	fetchAllUpcommingEvents() {
		this.upcommingEvent = {
			"title": "Upcoming Events",
			"header": "We all live in an age that belongs to the young at heart. Life that is becoming extremely fast",
			"events": [
				{
					"imageUrl": "assets/image/blog1.jpg",
					"title": "Spreading Peace to world",
					"dateTime": "Saturday, 5th may, 2018",
					"apartment": "Rocky beach Church",
					"location": "Santa monica, Los Angeles, USA"
				},
				{
					"imageUrl": "assets/image/blog2.jpg",
					"title": "Spread Happyness to world",
					"dateTime": "Saturday, 5th may, 2018",
					"apartment": "Rocky beach Church",
					"location": "Santa monica, Los Angeles, USA"
				},
				{
					"imageUrl": "assets/image/blog3.jpg",
					"title": "Spreading Light to world",
					"dateTime": "Saturday, 5th may, 2018",
					"apartment": "Rocky beach Church",
					"location": "Santa monica, Los Angeles, USA"
				}
			]
		};
	}

}
