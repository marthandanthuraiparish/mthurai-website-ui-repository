import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { AppConstants } from 'src/app/app.constants';


@Injectable({
	providedIn: 'root',
})
export class SermonsService {
	constructor(
		private http: HttpClient,
		private appConstants: AppConstants
	) {}
	
	fetchAllSermosInformation(): Observable<any> {
		return this.http.get<any>(this.appConstants.GET_SERMON_INFO)
			.pipe(
				retry(1),
				catchError(this.handleError)
			)
	}
	handleError(error) {
		let errorMessage = '';
		if (error.error instanceof ErrorEvent) {
			errorMessage = error.error.message;
		} else {
			errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
		}
		console.log(errorMessage);
		return throwError(errorMessage);
	}

	dummySermonInfo() {
		return [
			{
				"imageUrl": "assets/image/sermns.jpg",
				"header": "inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct standards especially in the workplace. That’s why it’s crucial that, as women, our behavior on the job is beyond reproach. inappropriate behavior is often laughed.",
				"title": "Did not find your Package? Feel free to ask us. We‘ll make it for you",
				"categories": "Travor James",
				"sermonSpeaker": "Prayer",
				"date": " 5th may, 2018"
			},
			{
				"imageUrl": "assets/image/sermns.jpg",
				"header": "inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct standards especially in the workplace. That’s why it’s crucial that, as women, our behavior on the job is beyond reproach. inappropriate behavior is often laughed.",
				"title": "Did not find your Package? Feel free to ask us. We‘ll make it for you",
				"categories": "Travor James",
				"sermonSpeaker": "Prayer",
				"date": " 5th may, 2018"
			},
			{
				"imageUrl": "assets/image/sermns.jpg",
				"header": "inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct standards especially in the workplace. That’s why it’s crucial that, as women, our behavior on the job is beyond reproach. inappropriate behavior is often laughed.",
				"title": "Did not find your Package? Feel free to ask us. We‘ll make it for you",
				"categories": "Travor James",
				"sermonSpeaker": "Prayer",
				"date": " 5th may, 2018"
			},
			{
				"imageUrl": "assets/image/sermns.jpg",
				"header": "inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct standards especially in the workplace. That’s why it’s crucial that, as women, our behavior on the job is beyond reproach. inappropriate behavior is often laughed.",
				"title": "Did not find your Package? Feel free to ask us. We‘ll make it for you",
				"categories": "Travor James",
				"sermonSpeaker": "Prayer",
				"date": " 5th may, 2018"
			},
			{
				"imageUrl": "assets/image/sermns.jpg",
				"header": "inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct standards especially in the workplace. That’s why it’s crucial that, as women, our behavior on the job is beyond reproach. inappropriate behavior is often laughed.",
				"title": "Did not find your Package? Feel free to ask us. We‘ll make it for you",
				"categories": "Travor James",
				"sermonSpeaker": "Prayer",
				"date": " 5th may, 2018"
			},
			{
				"imageUrl": "assets/image/sermns.jpg",
				"header": "inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct standards especially in the workplace. That’s why it’s crucial that, as women, our behavior on the job is beyond reproach. inappropriate behavior is often laughed.",
				"title": "Did not find your Package? Feel free to ask us. We‘ll make it for you",
				"categories": "Travor James",
				"sermonSpeaker": "Prayer",
				"date": " 5th may, 2018"
			},
		];
	}
}