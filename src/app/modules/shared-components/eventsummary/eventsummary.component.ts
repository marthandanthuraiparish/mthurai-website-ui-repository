import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-eventsummary',
  templateUrl: './eventsummary.component.html',
  styleUrls: ['./eventsummary.component.scss']
})
export class EventsummaryComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  eventsummary: any[] = [
    {eventid: '0001',eventname: '# Event 1',date: '01/07/2020' ,eventsummary: 'Our lady of lourdes festival 2020'},
    {eventid: '0002',eventname: '# Event 2',date: '15/08/2020' ,eventsummary: 'st aloysius school day events 2020'},
    {eventid: '0003',eventname: '# Event 3',date: '21/10/2020' ,eventsummary: 'Marthandanthurai sports day evets and celebrations'},
    {eventid: '0004',eventname: '# Event 4',date: '14/11/2020' ,eventsummary: 'Our Lady of Dolours Church Marthandanthurai events '},
    {eventid: '0005',eventname: '# Event 5',date: '25/12/2020' ,eventsummary: 'christmas festival in marthandanthurai 2020'}
  ]

}
