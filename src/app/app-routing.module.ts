import { NgModule } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import { HomeComponent } from './modules/site-module/home/home.component';
import { SermonsPageComponent } from './modules/site-module/sermons-page/sermons-page.component';
import { EventdetailsComponent } from './modules/shared-components/eventdetails/eventdetails.component';
import { EventsummaryComponent } from './modules/shared-components/eventsummary/eventsummary.component';
const routes: Routes = [
	{
		path: '',
		pathMatch: 'full',
		redirectTo: 'home'
	},
	{
		path: 'home',
		pathMatch: 'full',
		component: HomeComponent
	},
	{
		path: 'sermons',
		pathMatch: 'full',
		component: SermonsPageComponent,
	},
	{
		path: 'event',
		pathMatch: 'full',
		component: EventdetailsComponent
	},
	{
		path: 'event-details',
		pathMatch: 'full',
		component: EventsummaryComponent,
	},
	{
		path: '**',
		redirectTo: 'home'
	}
];
@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
